/*
 * Copyright (c) 2016, Fallen Studios
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by Fallen Studios.
 * 4. Neither the name of Fallen Studios nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FALLEN STUDIOS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FALLEN STUDIOS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.fallenstudios.fallenblocks.implementation.models;

import org.fallenstudios.fallenblocks.core.GenericBlock;
import org.fallenstudios.fallenblocks.core.ItemForm;
import org.fallenstudios.fallenblocks.implementation.InitImplementation;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import org.fallenstudios.fallenblocks.core.BlockHelper;

import java.util.Properties;

public class FallenBlock extends Block implements GenericBlock {

	BlockHelper helper;
	
	public FallenBlock(Properties blockProps, BlockHelper helper) {
        super(BlockHelper.getMaterial(blockProps.getProperty("material", "rock")));

        this.helper = helper;
		this.setCreativeTab(InitImplementation.blockTab);
	}

	@Override
	public ItemForm getItem() {
		return null;
	}

	/**
	 * Thanks to Mojang you need to implement {@link Block#setSoundType(SoundType)} here.
	 *
	 * @param sound
	 */
	@Override
	public void helper_setStepSound(SoundType sound) {
		this.setSoundType(sound);
	}
}
