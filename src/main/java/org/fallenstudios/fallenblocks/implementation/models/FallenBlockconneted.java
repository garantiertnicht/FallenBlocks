/*
 * Copyright (c) 2016, Fallen Studios
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by Fallen Studios.
 * 4. Neither the name of Fallen Studios nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FALLEN STUDIOS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FALLEN STUDIOS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.fallenstudios.fallenblocks.implementation.models;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import org.fallenstudios.fallenblocks.core.BlockHelper;
import org.fallenstudios.fallenblocks.core.GenericBlock;
import org.fallenstudios.fallenblocks.implementation.InitImplementation;

import java.util.Properties;

public class FallenBlockconneted extends Block implements GenericBlock {
    BlockHelper helper;
    final PropertyInteger connections = PropertyInteger.create("connections", 0, 63);

    public FallenBlockconneted(Properties blockProps, BlockHelper helper) {
        super(BlockHelper.getMaterial(blockProps.getProperty("material", "rock")));

        this.helper = helper;
        this.setCreativeTab(InitImplementation.blockTab);
    }

    /**
     * Thanks to Mojang you need to implement {@link Block#setSoundType(SoundType)} here.
     *
     * @param sound The sound to set to.
     */
    @Override
    public void helper_setStepSound(SoundType sound) {
        setSoundType(sound);
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, connections);
    }

    @SuppressWarnings("deprecation")
    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess world, BlockPos pos) {
        boolean[] connected = new boolean[6];

        /* UP    */  connected[0] = world.getBlockState(pos.up()).getBlock().equals(this);
        /* DOWN  */  connected[1] = world.getBlockState(pos.down()).getBlock().equals(this);
        /* NORTH */  connected[2] = world.getBlockState(pos.north()).getBlock().equals(this);
        /* SOUTH */  connected[3] = world.getBlockState(pos.south()).getBlock().equals(this);
        /* EAST  */  connected[4] = world.getBlockState(pos.east()).getBlock().equals(this);
        /* WEST  */  connected[5] = world.getBlockState(pos.west()).getBlock().equals(this);

        int connections = 0;

        for(boolean bool : connected) {
            if(bool) {
                connections |= 1;
            }

            connections <<= 1;
        }

        return state.withProperty(this.connections, connections);
    }
}
