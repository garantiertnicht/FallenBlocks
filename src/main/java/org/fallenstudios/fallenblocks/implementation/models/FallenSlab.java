/*
 * Copyright (c) 2016, Fallen Studios
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by Fallen Studios.
 * 4. Neither the name of Fallen Studios nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FALLEN STUDIOS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FALLEN STUDIOS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.fallenstudios.fallenblocks.implementation.models;

import org.fallenstudios.fallenblocks.core.GenericBlock;
import org.fallenstudios.fallenblocks.core.ItemForm;
import org.fallenstudios.fallenblocks.implementation.InitImplementation;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.SoundType;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSlab;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.fallenstudios.fallenblocks.core.BlockHelper;

import javax.annotation.Nullable;
import java.util.Properties;
import java.util.Random;

public class FallenSlab extends BlockSlab implements GenericBlock {
	private BlockHelper helper;
    private FallenSlabDouble doubleSlab;
    protected FallenSlabItem item;

    private static final PropertyBool variant = PropertyBool.create("variant");

	public FallenSlab(Properties blockProps, BlockHelper helper) {
        super(BlockHelper.getMaterial(blockProps.getProperty("material", "rock")));

        this.helper = helper;
		this.useNeighborBrightness = true;

		IBlockState iblockstate = this.blockState.getBaseState().withProperty(variant, false);

        if(!isDouble()) {
            this.setDefaultState(iblockstate.withProperty(HALF, EnumBlockHalf.BOTTOM));
            doubleSlab = new FallenSlabDouble(blockProps, helper, this);
            item = new FallenSlabItem(this, this, doubleSlab);
            new BlockHelper(helper, doubleSlab, "Double", blockProps);
        }
	}

	@Override
	public ItemForm getItem() {
		return isDouble() ? null : new ItemForm(item, this) ;
	}

	/**
	 * Thanks to Mojang you need to implement {@link Block#setSoundType(SoundType)} here.
	 *
	 * @param sound
	 */
	@Override
	public void helper_setStepSound(SoundType sound) {
        setSoundType(sound);
	}

    /**
     * Returns the slab block name with the type associated with it
     *
     * @param meta
     */
    @Override
    public String getUnlocalizedName(int meta) {
        return this.getRegistryName().toString();
    }

    @Override
    public boolean isDouble() {
        return false;
    }

    @Override
    public IProperty<?> getVariantProperty() {
        return variant;
    }

    @Override
    public Comparable<?> getTypeForItem(ItemStack stack) {
        return false;
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        if(isDouble()) {
            return 0;
        } else {
            if(state.getValue(HALF) == EnumBlockHalf.TOP) {
                return 0;
            } else {
                return 1;
            }
        }
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        if(isDouble()) {
            return getDefaultState();
        } else {
            if(meta == 1) {
                return getDefaultState().withProperty(HALF, EnumBlockHalf.TOP);
            } else {
                return getDefaultState();
            }
        }
    }

    public FallenSlabDouble getDoubleSlab() {
        return doubleSlab;
    }

    protected BlockStateContainer createBlockState() {
        return this.isDouble() ? new BlockStateContainer(this, variant) : new BlockStateContainer(this, HALF, variant);
    }

    @Nullable
    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return item;
    }

    @Override
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state) {
        return new ItemStack(item);
    }
}

class FallenSlabDouble extends FallenSlab {
    private FallenSlab parent;

    public FallenSlabDouble(Properties blockProps, BlockHelper helper, FallenSlab parent) {
        super(blockProps, helper);
        this.parent = parent;
    }

    @Override
    public boolean isDouble() {
        return true;
    }

    @Nullable
    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return parent.item;
    }

    @Override
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state) {
        return new ItemStack(parent.item);
    }
}

class FallenSlabItem extends ItemSlab {
    public FallenSlabItem(Block block, BlockSlab singleSlab, BlockSlab doubleSlab) {
        super(block, singleSlab, doubleSlab);
    }

    @Override
    public CreativeTabs getCreativeTab() {
        return InitImplementation.slabTab;
    }
}