/*
 * Copyright (c) 2016, Fallen Studios
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by Fallen Studios.
 * 4. Neither the name of Fallen Studios nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FALLEN STUDIOS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FALLEN STUDIOS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.fallenstudios.fallenblocks.implementation;

import org.fallenstudios.fallenblocks.core.Addon;
import org.fallenstudios.fallenblocks.core.CreativeTab;
import org.fallenstudios.fallenblocks.core.BlockHelper;
import org.fallenstudios.fallenblocks.core.Event;

import java.io.IOException;
import java.util.Properties;

public class InitImplementation {
    public static CreativeTab blockTab = new CreativeTab("FallenBlocks");
    public static CreativeTab stairTab = new CreativeTab("FallenStairs");
    public static CreativeTab slabTab = new CreativeTab("FallenSlabs");
    public static CreativeTab fenceTab = new CreativeTab("FallenFence");
    public static CreativeTab wallTab = new CreativeTab("FallenWall");
    public static CreativeTab miscTab = new CreativeTab("FallenMisc");

    public InitImplementation() throws IOException {
        Addon addon = Addon.newAddon("Fallen");

        Properties tabProps = new Properties();
        tabProps.load(this.getClass().getResourceAsStream("/assets/" + Addon.getModId().toLowerCase() + "/tabs.properties"));

        addon.createModelRegister("org.fallenstudios.fallenblocks.implementation.models");
        addon.addOwnBlockProccessedEventListener((Event.OwnBlockProcessed event) -> {
            BlockHelper helper = event.getHelper();

            String blockIdentifier = String.format("%s/%s.%s", helper.getName(), event.getHelper().getAddon(), helper.getType());
            System.out.println(blockIdentifier);

            if(blockIdentifier.equalsIgnoreCase(tabProps.getProperty("block")))
                blockTab.setIcon(helper.getItem());
            if(blockIdentifier.equalsIgnoreCase(tabProps.getProperty("stair")))
                blockTab.setIcon(helper.getItem());
            if(blockIdentifier.equalsIgnoreCase(tabProps.getProperty("slab")))
                slabTab.setIcon(helper.getItem());
            if(blockIdentifier.equalsIgnoreCase(tabProps.getProperty("fence")))
                fenceTab.setIcon(helper.getItem());
            if(blockIdentifier.equalsIgnoreCase(tabProps.getProperty("wall")))
                wallTab.setIcon(helper.getItem());
            if(blockIdentifier.equalsIgnoreCase(tabProps.getProperty("misc")))
                miscTab.setIcon(helper.getItem());
        });
    }
}
