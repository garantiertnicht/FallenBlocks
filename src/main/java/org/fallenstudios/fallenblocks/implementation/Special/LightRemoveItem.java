/*
 * Copyright (c) 2016, Fallen Studios
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by Fallen Studios.
 * 4. Neither the name of Fallen Studios nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FALLEN STUDIOS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FALLEN STUDIOS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.fallenstudios.fallenblocks.implementation.Special;

import org.fallenstudios.fallenblocks.implementation.InitImplementation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.fallenstudios.fallenblocks.core.FallenBlocks;

public class LightRemoveItem extends Item {
    public LightRemoveItem() {
        setCreativeTab(InitImplementation.miscTab);
        setUnlocalizedName("light_off");
    }
    @Override
    public ActionResult<ItemStack> onItemRightClick(ItemStack stack, World world, EntityPlayer ply, EnumHand hand) {
        double px = ply.posX;
        double py = ply.posY;
        double pz = ply.posZ;

        boolean removed = false;

        for(int i = (int) px - 7; i < px + 8; i++)
            for(int j = (int) py - 7; j < py + 8; j++)
                for(int k = (int) pz - 7; k < pz + 8; k++) {
                    BlockPos pos = new BlockPos(i, j, k);
                    if (world.getBlockState(pos).getBlock().equals(FallenBlocks.light)) {
                        world.setBlockToAir(pos);
                        removed = true;
                    }
                }

        if(!removed) {
            return new ActionResult<>(EnumActionResult.PASS, stack);
        }

        //Remove item in Survival ONLY when at least one block was removed.
        if(!ply.capabilities.isCreativeMode) {
            stack.stackSize--;
        }

        return new ActionResult<>(EnumActionResult.SUCCESS, stack);
    }
}
