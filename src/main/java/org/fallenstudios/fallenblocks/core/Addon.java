/*
 * Copyright (c) 2016, Fallen Studios
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by Fallen Studios.
 * 4. Neither the name of Fallen Studios nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FALLEN STUDIOS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FALLEN STUDIOS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.fallenstudios.fallenblocks.core;

import java.util.LinkedList;
import java.util.function.Consumer;

public class Addon {
    private boolean model;
    private String prefix;
    protected static LinkedList<Consumer<Event.FileLoaded>> floaded = new LinkedList<>();
    protected static LinkedList<Consumer<Event.BlockProcessed>> blockproccessed = new LinkedList<>();
    protected static LinkedList<EventHelper<Event.OwnBlockProcessed>> ownblockproccessed = new LinkedList<>();

    public class EventHelper<T> {
        private String prefix;
        private Consumer<T> consumer;

        private EventHelper(String prefix, Consumer<T> consumer) {
            this.prefix = prefix;
            this.consumer = consumer;
        }

        public String getPrefix() {
            return prefix;
        }

        public Consumer<T> getConsumer() {
            return consumer;
        }
    }

    private Addon(String prefix) {
        model = false;
        this.prefix = prefix;
    }

    public static Addon newAddon(String prefix) {
        return new Addon(prefix);
    }

    public ModelRegister createModelRegister(String pkg) {
        if(model) throw new IllegalStateException("Any Addon may ony request ONE Model Register!");
        return new ModelRegister(pkg, prefix);
    }

    public void addFileLoadedEventListener(Consumer<Event.FileLoaded> listener) {
        floaded.add(listener);
    }

    public void addBlockProccessedEventListener(Consumer<Event.BlockProcessed> listener) {
        blockproccessed.add(listener);
    }

    public void addOwnBlockProccessedEventListener(Consumer<Event.OwnBlockProcessed> listener) {
        ownblockproccessed.add(new EventHelper<>(prefix, listener));
    }

    public static String getModId() {
        return FallenBlocks.MODID;
    }

    public static LinkedList<Consumer<Event.FileLoaded>> getFloaded() {
        return floaded;
    }

    public static LinkedList<Consumer<Event.BlockProcessed>> getBlockproccessed() {
        return blockproccessed;
    }

    public static LinkedList<EventHelper<Event.OwnBlockProcessed>> getOwnblockproccessed() {
        return ownblockproccessed;
    }
}
