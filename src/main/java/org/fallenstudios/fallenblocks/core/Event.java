/*
 * Copyright (c) 2016, Fallen Studios
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by Fallen Studios.
 * 4. Neither the name of Fallen Studios nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FALLEN STUDIOS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FALLEN STUDIOS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.fallenstudios.fallenblocks.core;

import java.util.Properties;
import java.util.function.Consumer;

public class Event {
    public static class FileLoaded {
        private Properties blockProps;
        private String name;
        private String[] types;

        protected FileLoaded(Properties blockProps, String name, String types[]) {
            this.blockProps = blockProps;
            this.name = name;
            this.types = types;
        }

        public Properties getBlockProperties() {
            return blockProps;
        }

        public void addBlockPropertie(String key, String val) {
            if(blockProps.contains(key))
                throw new IllegalStateException(String.format("Property %s allredy added!", key));
            blockProps.setProperty(key, val);
        }

        public String getFileName() {
            return name;
        }

        public String[] getBlockTypes() {
            return types;
        }

        protected FileLoaded fire() {
            for(Consumer<FileLoaded> listener : Addon.getFloaded())
                listener.accept(this);
            return this;
        }
    }

    public static class BlockProcessed {
        private BlockHelper helper;

        protected BlockProcessed(BlockHelper helper) {
            this.helper = helper;
        }

        public BlockHelper getHelper() {
            return helper;
        }

        protected BlockProcessed fire() {
            for(Consumer<BlockProcessed> listener : Addon.getBlockproccessed())
                listener.accept(this);
            return this;
        }
    }

    public static class OwnBlockProcessed {
        private BlockHelper helper;

        protected OwnBlockProcessed(BlockHelper helper) {
            this.helper = helper;
        }

        public BlockHelper getHelper() {
            return helper;
        }

        protected OwnBlockProcessed fire(String prefix) {
            for(Addon.EventHelper<OwnBlockProcessed> helper : Addon.getOwnblockproccessed())
                if(helper.getPrefix().equals(prefix))
                    helper.getConsumer().accept(this);
            return this;
        }
    }
}
