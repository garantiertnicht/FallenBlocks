/*
 * Copyright (c) 2016, Fallen Studios
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by Fallen Studios.
 * 4. Neither the name of Fallen Studios nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FALLEN STUDIOS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FALLEN STUDIOS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.fallenstudios.fallenblocks.core;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

public class BlockHelper {
    private String name;
    private String type;
    private String addon;
    private Properties blockProps;
    private GenericBlock genericBlock;
    private Block block;

    private boolean legacy;
    private boolean devLog;

    private static FallenBlocks Core;

    /**
     * The block Type passed to the constructor wasn't found.
     */
    public class InvalidBlockTypeException extends Exception {
        private InvalidBlockTypeException(String msg) {
            super(String.format("%s/%s: %s", name, type, msg), null, false, true);
        }

        private InvalidBlockTypeException(String msg, Throwable cause) {
            super(String.format("%s/%s: %s", name, type, msg), cause, false, true);
        }
    }

    /**
     * The Texture wish you liked to register does not exists.
     */
    public class NoSuchTexture extends Exception {
        private NoSuchTexture(String textName) {
            super(String.format("%s/%s: Texture %s is missing!", name, type, textName), null, false, true);
        }
    }

    /**
     * Creates a new Block
     * @param typestring The type of the Block. The Class will be org.imperiumstudios.fallenblocks.implementation.models.IMP(TYPE)
     *             with type lowercase EXCEPT the first letter.
     * @param name The name of the new Block.
     * @param blockProps The block Properties to apply
     * @throws InvalidBlockTypeException If the type was invalid
     */
    protected BlockHelper(String typestring, String name, Properties blockProps) throws InvalidBlockTypeException {
        this.name = name;

        String split[] = typestring.split("\\.");
        if(split.length == 2) {
            addon = split[0];
            type = split[1];
            legacy = false;
        } else if(split.length == 1) {
            addon = "Fallen";
            type = split[0];
            info("This is an legacy Block. Please update your Blockfiles.");
            legacy = true;
        } else {
            throw new InvalidBlockTypeException("A blocktype should be like addonprefix.type!");
        }

        char first = type.charAt(0);
        type = type.toLowerCase().substring(1);
        type = Character.toUpperCase(first) + type;

        if(Boolean.valueOf(blockProps.getProperty("devFunc001"))) {
            info("Brutal Logging enabled!");
            devLog = true;
        } else
            devLog = false;

        Object o;
        Class imp;
        String pkg = null;

        for(ModelRegister reg : ModelRegister.registers)
            if(addon.equalsIgnoreCase(reg.getPrefix())) {
                this.addon = reg.getPrefix();
                pkg = reg.getPackage() + "." + addon + type;
                break;
            }

        if(pkg == null) {
            throw new InvalidBlockTypeException(String.format("You need an additional Addon providing Block Types with prefix %s!", addon));
        }

        try {
            imp = Class.forName(pkg);
        } catch (ClassNotFoundException e) {
            throw new InvalidBlockTypeException(String.format("There is no such Block Type called %s.%s!", addon, type));
        }

        Class inter[] = imp.getInterfaces();
        boolean flag = false;

        for(Class a : inter) {
            if(a == GenericBlock.class)
                flag = true;
                break;
            }

        if(!flag)
            throw new InvalidBlockTypeException("Class exists but is no GenericBlock!");

        flag = false;
        Class temp = imp;

        while(temp != Object.class) {
            temp = temp.getSuperclass();
            if(temp == Block.class) {
                flag = true; //result is unused
                break;
            }
        }

        if(!flag)
            throw new InvalidBlockTypeException("Class exists and implements GenericBlock, but is no instanceof Block!");

        blockProps.setProperty("name", name + type);
        this.blockProps = blockProps;

        try {
            o = imp.getConstructor(Properties.class, this.getClass()).newInstance(blockProps, this);
        } catch (InstantiationException e) {
            throw new InvalidBlockTypeException("Class exists and implements GenericBlock, but the construction failed!");
        } catch (IllegalAccessException e) {
            throw new InvalidBlockTypeException("Class exists and implements GenericBlock, but the construction can not called (change to public)!");
        } catch (InvocationTargetException e) {
            throw new InvalidBlockTypeException("Class exists and implements GenericBlock, but the construction thrown an error!", e);
        } catch (NoSuchMethodException e) {
            throw new InvalidBlockTypeException("Class exists and implements GenericBlock, but has no matchin Constructor (Properties, BlockHelper)!");
        }

        if(devLog)
            info("Block successfully initalized!");

        genericBlock = (GenericBlock) o;
        block = (Block) o;

        block.setRegistryName(name + addon + type);
        block.setUnlocalizedName(name + addon + type);
        genericBlock.helper_setStepSound(getSoundType(blockProps.getProperty("sound", "stone")));

        try {
            block.setHardness(Float.valueOf(blockProps.getProperty("hardness", "2")));
        } catch (NumberFormatException exc) {
            warn("Value in hardness is NO parsable Float!");
            block.setHardness(2);
        }

        if(blockProps.getProperty("blast") != null)
            try {
                block.setResistance(Float.valueOf(blockProps.getProperty("blast")));
            } catch (NumberFormatException exc) {
                warn("Value in blast is NO parsable Float!");
            }

        if(block.isVisuallyOpaque())
            try {
                block.setLightLevel(Float.valueOf(blockProps.getProperty("light", "0.0F")));
            } catch (NumberFormatException exc) {
                warn("Value in light is NO parsable Float!");
            }

        GameRegistry.registerBlock(block);

        if(genericBlock.getItem() != null) {
            Item item = genericBlock.getItem().getItem();
            item.setUnlocalizedName(name + addon + type + "Item");
            GameRegistry.registerItem(item, name + addon + type + "Item");
            if(genericBlock.getItem().getBlock() != null) {
                Core.getSidedProxy().RegisterItemToBlockRendering(item, block);
            } else {
                Core.addItemToRegisterLater(item);
            }
        } else {
            Core.getSidedProxy().RegisterItemBlockRendering(block);
        }

        if(devLog)
            info("Block Registered!");
    }

    public BlockHelper(BlockHelper parent, Object blockObject, String appendix, Properties blockProps) {
        genericBlock = (GenericBlock) blockObject;
        block = (Block) blockObject;

        this.blockProps = blockProps;

        this.addon = parent.addon;
        this.name = parent.name;
        this.type = parent.type;
        this.devLog = parent.devLog;
        this.legacy = parent.legacy;

        block.setRegistryName(name + addon + type + appendix);
        block.setUnlocalizedName(name + addon + type + appendix);
        genericBlock.helper_setStepSound(getSoundType(blockProps.getProperty("sound", "stone")));

        try {
            block.setHardness(Float.valueOf(blockProps.getProperty("hardness", "2")));
        } catch (NumberFormatException exc) {
            warn("Value in hardness is NO parsable Float!");
            block.setHardness(2);
        }

        if(blockProps.getProperty("blast") != null)
            try {
                block.setResistance(Float.valueOf(blockProps.getProperty("blast")));
            } catch (NumberFormatException exc) {
                warn("Value in blast is NO parsable Float!");
            }

        if(block.isVisuallyOpaque())
            try {
                block.setLightLevel(Float.valueOf(blockProps.getProperty("light", "0.0F")));
            } catch (NumberFormatException exc) {
                warn("Value in light is NO parsable Float!");
            }

        GameRegistry.registerBlock(block);

        if(genericBlock.getItem() != null) {
            Item item = genericBlock.getItem().getItem();
            item.setUnlocalizedName(name + addon + type + "Item");
            GameRegistry.registerItem(item, name + addon + type + "Item");
            if(genericBlock.getItem().getBlock() != null) {
                Core.getSidedProxy().RegisterItemToBlockRendering(item, block);
            } else {
                Core.addItemToRegisterLater(item);
            }
        } else {
            Core.getSidedProxy().RegisterItemBlockRendering(block);
        }

        if(devLog)
            info("Block Registered!");
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getAddon() {
        return addon;
    }

    public Properties getBlockProperties() {
        return blockProps;
    }

    public Block getBlock() {
        return block;
    }

    public GenericBlock genericBlock() {
        return genericBlock;
    }

    /**
     * Gets the Item for a block.
     * @return An Item. Who is surprised?
     */
    public Item getItem() {
        return genericBlock.getItem() == null ? Item.getItemFromBlock(block) : genericBlock.getItem().getItem();
    }

    public void info(String msg) {
        FallenBlocks.log.info(String.format("%s/%s.%s: %s", name, addon, type, msg));
    }

    public void warn(String msg) {
        FallenBlocks.log.warn(String.format("%s/$s.%s: %s", name, addon, type, msg));
    }

    /**
     * Looks up an Material from a given String.
     * @param s The String-Representation of the Material.
     * @return Even more surprisingly, an Material!
     */
    public static Material getMaterial(String s) {
        String materialS[]  = {"air", "grass", "ground", "wood", "rock", "iron", "anvil", "water", "lava", "leaves", "plants", "vine", "sponge", "cloth",
                "fire", "sand", "circuits", "carpet", "glass", "redstoneLight", "tnt", "coral", "ice", "packedIce", "snow", "craftedSnow", "cactus",
                "clay", "gourd", "dragonEgg", "portal", "cake", "web", "piston"};

        Material material[] = {Material.AIR, Material.GRASS, Material.GROUND, Material.WOOD, Material.ROCK, Material.IRON, Material.ANVIL, Material.WATER,
                Material.LAVA, Material.LEAVES, Material.PLANTS, Material.VINE, Material.SPONGE, Material.CLOTH, Material.FIRE, Material.SAND,
                Material.CIRCUITS, Material.CARPET, Material.GLASS, Material.REDSTONE_LIGHT, Material.TNT, Material.CORAL, Material.ICE, Material.PACKED_ICE,
                Material.SNOW, Material.CRAFTED_SNOW, Material.CACTUS, Material.CLAY, Material.GOURD, Material.DRAGON_EGG, Material.PORTAL, Material.CAKE,
                Material.WEB, Material.PISTON};

    	for(int i = 0; i < Math.min(materialS.length, material.length); i++) {
            if (materialS[i].equalsIgnoreCase(s))
                return material[i];
        }

        return Material.ROCK;
    }

    /**
     * Looks up an SoundType from a given String.
     * @param s The String-Representation of the SoundType.
     * @return A small bananna ridden by a giant ape, falling because of damn grass.
     */
    public static SoundType getSoundType(String s) {
        String soundS[] = {"WOOD", "GROUND", "PLANT", "STONE", "METAL", "GLASS", "CLOTH", "SAND", "SNOW", "LADDER", "ANVIL", "SLIME"};
        SoundType sound[] = {SoundType.WOOD, SoundType.GROUND, SoundType.PLANT, SoundType.STONE, SoundType.METAL, SoundType.GLASS, SoundType.CLOTH, SoundType.SAND, SoundType.SNOW, SoundType.LADDER, SoundType.ANVIL, SoundType.SLIME};

        for (int i = 0; i < Math.min(soundS.length, sound.length); i++) {
            if (soundS[i].equals(s.toUpperCase()))
                return sound[i];
        }

        return SoundType.STONE;
    }

    /**
     * Looks up an SoundType from a given String.
     * @param s The String-Representation of the SoundType.
     * @return A small bananna ridden by a giant ape, falling because of damn grass.
     */
    public static MapColor getMapColor(String s) {
        String colorsS[] = {"AIR", "GRASS", "SAND", "CLOTH", "TNT", "ICE", "IRON", "FOLIAGE", "SNOW", "CLAY", "DIRT", "STONE", "WATER", "WOOD", "QUARTZ", "ADOBE", "MAGENTA", "LIGHT", "YELLOW", "LIME", "PINK", "GRAY", "SILVER", "CYAN", "PURPLE", "BLUE", "BROWN", "GREEN", "RED", "BLACK", "GOLD", "DIAMOND", "LAPIS", "EMERALD", "OBSIDIAN", "NETHERRACK"};
        MapColor colors[] = {MapColor.AIR, MapColor.GRASS, MapColor.SAND, MapColor.CLOTH, MapColor.TNT, MapColor.ICE, MapColor.IRON, MapColor.FOLIAGE, MapColor.SNOW, MapColor.CLAY, MapColor.DIRT, MapColor.STONE, MapColor.WATER, MapColor.WOOD, MapColor.QUARTZ, MapColor.ADOBE, MapColor.MAGENTA, MapColor.LIGHT_BLUE, MapColor.YELLOW, MapColor.LIME, MapColor.PINK, MapColor.GRAY, MapColor.SILVER, MapColor.CYAN, MapColor.PURPLE, MapColor.BLUE, MapColor.BROWN, MapColor.GREEN, MapColor.RED, MapColor.BLACK, MapColor.GOLD, MapColor.DIAMOND, MapColor.LAPIS, MapColor.EMERALD, MapColor.OBSIDIAN, MapColor.NETHERRACK};

        for (int i = 0; i < Math.min(colorsS.length, colors.length); i++) {
            if (colorsS[i].equals(s.toUpperCase()))
                return colors[i];
        }

        return MapColor.AIR;
    }

    /**
     * Sets the Core. Seemnd to be better then having another var every call.
     * @param newCore FallenBlocks, wish is *HOPEFULLY* calling this.
     */
    public static void init(FallenBlocks newCore) {
        Core = newCore;
    }
}