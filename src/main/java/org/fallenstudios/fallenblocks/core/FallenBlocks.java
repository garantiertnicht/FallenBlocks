/*
 * Copyright (c) 2016, Fallen Studios
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by Fallen Studios.
 * 4. Neither the name of Fallen Studios nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY FALLEN STUDIOS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FALLEN STUDIOS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.fallenstudios.fallenblocks.core;

import org.fallenstudios.fallenblocks.core.proxy.Proxy;
import org.fallenstudios.fallenblocks.implementation.InitImplementation;
import org.fallenstudios.fallenblocks.implementation.Special.Light;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

@Mod(modid = FallenBlocks.MODID, version = FallenBlocks.VERSION, name = FallenBlocks.NAME)
public class FallenBlocks {
	
	private boolean GUIFORBIDDEN = false;
	
	public Utils utils = new Utils();
	
	public static final String NAME = "FallenBlocks"; //Used in the GUI parts of the mod
	public static final String MODID = "fallenblocks";
	public static final String VERSION = "1.0";

	@SidedProxy(clientSide = "org.fallenstudios.fallenblocks.core.proxy.Client", serverSide = "org.fallenstudios.fallenblocks.core.proxy.Server")
	private static Proxy sidedProxy;

	public static Light light;
	private ArrayList<Item> itemsToRegister;

	public static org.apache.logging.log4j.Logger log;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent e) throws IOException, URISyntaxException {
		log = e.getModLog();
		itemsToRegister = new ArrayList<>();
		
		if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) GUIFORBIDDEN = true;
		//patchJar();
		new InitImplementation();

		InputStream inputStream;
		BlockHelper.init(this);

		for(String blockFile: utils.getResourceFolderContent("assets/fallenblocks/blocks/")) {
			if(blockFile.equals("")) continue;

			inputStream = getClass().getClassLoader().getResourceAsStream("assets/fallenblocks/blocks/" + blockFile);

			if(inputStream != null) {
				Properties blockProps = new Properties();
				blockProps.load(inputStream);

				String[] blockTypes = blockProps.getProperty("types", "block").replace(" ", "").split(",");
				String name = blockFile.replace(".properties", "");

				blockProps = new Event.FileLoaded(blockProps, blockFile, blockTypes).fire().getBlockProperties();

				for(String blockType: blockTypes) {
					BlockHelper helper;
					try {
						helper = new BlockHelper(blockType, name, blockProps);
					} catch(BlockHelper.InvalidBlockTypeException exc) {
						exc.printStackTrace();
						continue;
					}

					new Event.OwnBlockProcessed(helper).fire(helper.getAddon());
					new Event.BlockProcessed(helper).fire();

				}
			} else throw new FileNotFoundException("property file '" + blockFile + "' not found in the classpath");
		}

		//light = new Light();
		//GameRegistry.registerBlock(light, "light");

		//LightRemoveItem lightOff = new LightRemoveItem();
		//LightItem lightOn = new LightItem();

		//GameRegistry.registerItem(lightOn, "lightOn");
		//GameRegistry.registerItem(lightOff, "lightOff");

		//Glowstone in the middle and the edges, glass in the free slots
		//GameRegistry.addShapedRecipe(new net.minecraft.item.ItemStack(lightOn, 2), "OgO", "gOg", "OgO", 'O', Blocks.GLOWSTONE, 'g', Blocks.GLASS);

		//Glowstone in the middle, surrounded by cleanstone
		//GameRegistry.addShapedRecipe(new net.minecraft.item.ItemStack(lightOff, 1), "SSS", "SOS", "SSS", 'S', Blocks.STONE, 'O', Blocks.GLOWSTONE);
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent e) {
		for (Item item : itemsToRegister) {
			sidedProxy.RegisterItemRendering(item);
		}
	}
    
	public void patchJar() throws IOException {
		URL website = new URL("http://block.imperium1871.de/version");
		ReadableByteChannel rbc = Channels.newChannel(website.openStream());

		Scanner scan = new Scanner(rbc);
		String v1 = scan.nextLine();
		scan.close();

		InputStream is = this.getClass().getResourceAsStream("/assets/fallenblocks/version");
		scan = new Scanner(is);

		String v2 = scan.nextLine();
		scan.close();

		if(v1.equals(v2) || v1.equals("0")) {
			log.info(String.format("Client is up-to-secound; Currently installed version is %s and should be %s", v2, v1));
			return;
		}

		log.info(String.format("Running Client update; Updating from version %s to version %s", v2, v1));

		String jvm_location;

		if(System.getProperty("os.name").startsWith("Win")) jvm_location = System.getProperties().getProperty("java.home") + File.separator + "bin" + File.separator + "java.exe";
		else jvm_location = System.getProperties().getProperty("java.home") + File.separator + "bin" + File.separator + "java";

		Runtime.getRuntime().exec(new String[] {jvm_location, "-jar", Minecraft.getMinecraft().mcDataDir + File.separator + "AssetsLoader.jar", Minecraft.getMinecraft().mcDataDir.getPath(), "http://block.imperium1871.de/assets.zip", "FallenBlocks-" + VERSION, MODID.toLowerCase(), v1, String.valueOf(GUIFORBIDDEN)});
		
		FMLCommonHandler.instance().exitJava(0, false);
	}

	public Proxy getSidedProxy() {
		return sidedProxy;
	}

	public void addItemToRegisterLater(Item item) {
		itemsToRegister.add(item);
	}
}
